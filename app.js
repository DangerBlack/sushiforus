var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
//var sassMiddleware = require('node-sass-middleware');

var index = require('./routes/index');
var room = require('./routes/room');
var about = require('./routes/about');
var contacts = require('./routes/contacts');
var partner = require('./routes/partner');
var info = require('./routes/info');
var cookie = require('./routes/cookie');

var locale = require('locale');
var langUtils = require('./utils/language');

var db = require('sqlite');
var dbPromise = db.open(path.join(__dirname, './db','sm.sqlite'), { Promise });
var database = require('./utils/database');
var passport = require('passport');
var Strategy = require('passport-local').Strategy;


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hjs');

app.use('/js', express.static(__dirname + '/node_modules/jquery/dist')); // redirect JS jQuery
app.use('/mdbootstrap/js', express.static(__dirname + '/node_modules/mdbootstrap/js')); // redirect JS jQuery
app.use('/popper.js', express.static(__dirname + '/node_modules/dist')); // redirect JS jQuery
app.use('/datepicker-bootstrap', express.static(__dirname + '/node_modules/datepicker-bootstrap')); // redirect JS jQuery
app.use('/js/d3.min.js', express.static(__dirname + '/node_modules/d3/build/d3.min.js'));
app.use('/js/d3.js', express.static(__dirname + '/node_modules/d3/build/d3.js'));
app.use('/robots.txt', express.static(__dirname + '/public'));


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
/*app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true, // true = .sass and false = .scss
  sourceMap: true
}));*/


app.use(express.static(path.join(__dirname, 'public')));


app.use('/', index);
app.use('/', room);

app.use('/', cookie);
app.use('/info', info);
app.use('/contacts', contacts);
app.use('/partnership', partner);
app.use('/about', about);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  //res.render('error');
  var jsonLang = langUtils.getLenguageJson(locale,path,__dirname+"/tmp",req);
  res.render('error', jsonLang);
});



module.exports = app;
