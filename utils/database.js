module.exports = {
    getQuery: getQuery,
    getRoomFromKey: function (db,key){
        return getQuery(db,"SELECT r.id FROM room AS r WHERE key = (?)",[key]);
    },
    insertRoom: function(db,key){
      return insertQuery(db,"INSERT INTO room (key) VALUES (?)",[key])
    },
    getOrderingByIdRoom: function(db,idroom){
      return allQuery(db,"SELECT n,SUM(quantity) AS s FROM ordering WHERE idroom=(?) GROUP BY n",[idroom]);
    },
    getOrderingByIdRoomGroupUserkey: function(db,idroom){
      return allQuery(db,"SELECT n,SUM(quantity) AS s,userkey FROM ordering WHERE idroom=(?) GROUP BY n,userkey",[idroom]);
    },
    insertOrder: function(db,idroom,n,quantity,userkey){
      return insertQuery(db,"INSERT INTO ordering (idroom,n,quantity,userkey) VALUES (?,?,?,?)",[idroom,n,quantity,userkey]);
    },
    getOrderingByN: function(db,idroom,n){
      return getQuery(db,"SELECT n,SUM(quantity) AS s FROM ordering WHERE idroom=(?) AND n=(?) GROUP BY n",[idroom,n]);
    },
    getOrdering: function(db,idroom){
      return allQuery(db,"SELECT n,SUM(quantity) AS s FROM ordering WHERE idroom=(?) GROUP BY n",[idroom]);
    },
    getClient: function(db,idroom){
      return allQuery(db,"SELECT id,client FROM guest WHERE idroom=(?) AND status = 1",[idroom])
    },
    insertClient: function(db,idclient,idroom,clientIp){
      return insertQuery(db,"INSERT INTO guest (client,idroom,ip,status) VALUES (?,?,?,?)",[idclient,idroom,clientIp,1]);
    },
    updateClient: function(db,client){
      return updateQuery(db,"UPDATE guest SET status = 0 WHERE client = ?",[client]);
    },
    getFortuneCookies: function(db,lang){
      return allQuery(db,"SELECT id,text FROM cookie WHERE language = (?)",[lang])
    },
    deleteUnusedRoom: function(db){
      return deleteQuery(db,'DELETE FROM room WHERE id IN (SELECT r.id from room as r LEFT JOIN ordering as o ON r.id = o.idroom WHERE date("now","-7 days")>r.time and o.id is null GROUP BY r.id)');
    },
    insertPartnerMail: function(db,mail){
      return insertQuery(db,"INSERT INTO partner (mail) VALUES (?)",[mail]);
    }
}

/**
@search the SQL query
@keys and array of key to substitute to the ? in the search
@success the success function
@failure the failure function
**/
function getQuery(db,search,keys){
    return db.get(search,
      keys
    );
}
function allQuery(db,search,keys){
return db.all(search,
    keys
    );
}
function insertQuery(db,query,keys){
console.log("esiste?");
return db.run(query,
    keys
    );
  }
function updateQuery(db,query,keys){
return db.run(query,
    keys
    );
  }
function deleteQuery(db,query,keys){
return db.run(query,
    keys
    );
}
