module.exports = {
    isAndroid: function(req){
      var ua = req.headers['user-agent'].toLowerCase();
      console.log("UA:"+ua);
      var isAndroid = ua.indexOf("android") > -1;
      if(isAndroid){
        return true;
      }else{
        return false;
      }
    },
    getLenguageJson: function (locale,path,__dirname,request){
          var supported = new locale.Locales(["en","en_US","it","es","sv"]);
          var locales = new locale.Locales(request.headers["accept-language"],'en');
          var l = locales.best(supported).language;
          var language = require(path.join(__dirname, '../public/lang',l+".json"));
          language['IS_ANDROID'] = module.exports.isAndroid(request);
          return language;
    }

}
