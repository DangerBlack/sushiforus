var express = require('express');
var router = express.Router();
var path = require('path');
var locale = require('locale');
var langUtils = require('../utils/language');
global.Promise=require("bluebird");
var db = require('sqlite');
var crypto = require('crypto');

var dbPromise = db.open(path.join(__dirname, '../db','sm.sqlite'), { Promise })
var database = require('../utils/database');

router.get('/robots.txt', function(req, res, next) {
    //console.log("Is Android?:"+langUtils.isAndroid(req));
    var jsonLang = langUtils.getLenguageJson(locale,path,__dirname,req);
    res.render('robots', jsonLang);
});

router.get('/', function(req, res, next) {
    //console.log("Is Android?:"+langUtils.isAndroid(req));
    var jsonLang = langUtils.getLenguageJson(locale,path,__dirname,req);
    res.render('index', jsonLang);
});

router.get('/enter', function(req, res, next) {
    var jsonLang = langUtils.getLenguageJson(locale,path,__dirname,req);
    res.render('enter', jsonLang);
});

module.exports = router;
