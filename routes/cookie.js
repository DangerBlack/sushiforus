var express = require('express');
var router = express.Router();
var path = require('path');
var locale = require('locale');
var langUtils = require('../utils/language');
global.Promise=require("bluebird");
var db = require('sqlite');
var crypto = require('crypto');

var dbPromise = db.open(path.join(__dirname, '../db','sm.sqlite'), { Promise })
var database = require('../utils/database');
//var language = require(path.join(__dirname, '../public/lang','english.json'));
/* GET home page. */
router.get('/room/:key/cookie', function(req, res, next) {
  var jsonLang = langUtils.getLenguageJson(locale,path,__dirname,req);
  database.getFortuneCookies(db,"english").then(function(cookies){
      var max = cookies.length;
      var n = Math.floor(Math.random()*max);
      res.send('<div class="alert alert-default white" role="alert">'+cookies[n].text+'</div>');
  });

});

module.exports = router;
