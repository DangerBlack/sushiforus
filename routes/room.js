
var express = require('express');
var router = express.Router();
var path = require('path');
var locale = require('locale');
var langUtils = require('../utils/language');
global.Promise=require("bluebird");
var db = require('sqlite');
var crypto = require('crypto');

var dbPromise = db.open(path.join(__dirname, '../db','sm.sqlite'), { Promise })
var database = require('../utils/database');


router.get('/room/create', function(req, res, next) {
    var key = crypto.randomBytes(3).toString('hex').toUpperCase();
    //500 collision over 100000 test
    console.log(key);
    Promise.all([database.insertRoom(db,key)]).then(function(room){
      room = room[0];
      res.redirect('/room/'+key+"/");
      //res.send(key);
    }).catch(function(err){
      //Try to make more space deleting unused room
      database.deleteUnusedRoom(db).then(function(response){
          res.redirect('/room/create');
      }).catch(function(error){
          var jsonLang = langUtils.getLenguageJson(locale,path,__dirname,req);
          res.render('roomerror', jsonLang);
      });
    });
});
/* GET home page. */
router.get('/room/:key', function(req, res, next) {
    if (!req.url.endsWith('/')) {
      res.redirect(301, req.originalUrl + '/')
    }

    var jsonLang = langUtils.getLenguageJson(locale,path,__dirname,req);
    console.log("Richiesta stanza:"+req.params.key);
    database.getRoomFromKey(db,req.params.key).then(function(room){
      if(room!=null){
        jsonLang.key = req.params.key;
        res.render('room', jsonLang);
      }else {
        res.render('roomerror', jsonLang);
      }
    }).catch(function(err){

    });
});


router.get('/room/:key/close', function(req, res, next) {
    var jsonLang = langUtils.getLenguageJson(locale,path,__dirname,req);
    console.log("Richiesta stanza:"+req.params.key);
    database.getRoomFromKey(db,req.params.key).then(function(room){
      if(room!=null){
        Promise.all([
                      database.getOrderingByIdRoom(db,room.id),
                      database.getOrderingByIdRoomGroupUserkey(db,room.id)
                    ]).then(function(result){
          jsonLang.key = req.params.key;
          jsonLang.ordering = result[0];
          jsonLang.personalordering = result[1];
          res.render('close', jsonLang);
        });
      }else{
        res.render('roomerror', jsonLang);
      }
    });
});

router.get('/room/:key/reseed', function(req, res, next) {
    var jsonLang = langUtils.getLenguageJson(locale,path,__dirname,req);
    console.log("Richiesta stanza:"+req.params.key);
    database.getRoomFromKey(db,req.params.key).then(function(room){
      if(room!=null){
        var key = crypto.createHash('sha256')
                       .update(req.params.key)
                       .digest('hex').toUpperCase();
        key = key.substr(0,6);
        console.log(key);
        Promise.all([database.insertRoom(db,key)]).then(function(room){
          res.redirect('/room/'+key+"/");
        }).catch(function(err){
          res.redirect('/room/'+key+"/");
        });
      }else{
        res.render('roomerror', jsonLang);
      }
    });
});

router.post('/room/:key/insertorder', function(req, res, next) {
    database.getRoomFromKey(db,req.params.key).then(function(room){
      var id = room.id;
      database.insertOrder(db,id,req.body.n,req.body.quantity,req.body.userkey).then(function(result){
        Promise.all([
                      database.getOrderingByN(db,id,req.body.n),
                      database.getClient(db,id)
                    ]).then(function(response){
                      var ordering = response[0];
                      var client = response[1];
                      var io = global.socketIO;
                      res.send(JSON.stringify(ordering));
                      var message = ordering;
                      for(let i=0;i<client.length;i++){
                        if (io.sockets.connected[client[i].client]) {
                            console.log("sparo mex :"+client.client);
                            io.sockets.connected[client[i].client].emit('new',JSON.stringify(message));
                            console.log("sparato");
                         }else{
                             database.updateClient(db,client[i].client).then(function(result){

                             });
                         }
                      }
        }).catch(function(e){
          console.log("e1"+e);
        });
    }).catch(function(e){
      console.log("e2:"+e);
    });
  });
});

router.get('/room/:key/ordering', function(req, res, next) {
  console.log("qui!"+req.params.key);
  database.getRoomFromKey(db,req.params.key).then(function(room){
    console.log(room);
      database.getOrdering(db,room.id,req.body.n).then(function(ordering){
        console.log(ordering);
        res.send(JSON.stringify(ordering));
      }).catch(function(e){
        console.log(e);
      });
  }).catch(function(error){
    console.log(error);
  });
});


module.exports = router;
