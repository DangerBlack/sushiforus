/* Set the width of the side navigation to 250px and the left margin of the page content to 250px */
function openNav() {
    document.getElementById("mySidenav").style.width = "80%";
    if($("#black").length===0){
      $("#mySidenav").after('<div id="black" onclick="closeNav()" ></div>');
    }else{
      $("#black").css("display","");
    }
    $("#mySidenav").on("swipeleft",function(){
      console.log("Swping on");
      closeNav();
    });
    $("#black").on("swipeleft",function(){
      console.log("Swping on");
      closeNav();
    });
    //document.getElementById("main").style.marginLeft = "250px";
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    $("#black").css("display","none");
    //document.getElementById("main").style.marginLeft = "0";
}
function initTip(key){
	if(isNewUser()){
    console.log("new user");
    newUser();
    location.href = "/info/"+key;
		//show tutorial
	}
}

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("scrolltop").style.display = "block";
    } else {
        document.getElementById("scrolltop").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}

var max_number = 152;
function addOrder(element,n){
  if(element!=null){
	   $(element).removeClass("deleted");
	   $(element).addClass("selected");
  }
	$.post("insertorder",{"n":n,"quantity":1,"userkey":getUserkey()},function(data){
			   var js= JSON.parse(data);
			   console.log(js);
			   console.log(js.s);
         if($("#small"+js.n).length>0){
			        $("#small"+js.n).text("x"+js.s);
         }else{
           //$("#numbers").append('<button class="maki addOrder'+js.n+' selected" value="'+js.n+'">'+js.n+'<span id="small'+js.n+'" class="quantity">X'+js.s+'</span></button>');
           addElementButton(js.n,js.s);
           addEventButton(""+js.n)
         }
	});
}
function removeOrder(n){
	//$(this).removeClass("btn-default");
	//$(this).addClass("btn-danger");
	$.post("insertorder",{"n":n,"quantity":-1,"userkey":getUserkey()},function(data){
			   var js= JSON.parse(data);
			   console.log(js);
			   console.log(js.s);
			   $("#small"+js.n).text("x"+js.s);
	});
}
function selectLetterModal(element,n){
  console.log("prova");
  /*var custombutton="testo";
  var reduceportion="testo";
  var deletes="testo";*/
  var customize='';
console.log(n);
  if(!isNaN(n)){
    customize='<label>'+custombutton+' '+n+'</label><br />'+
    '<div class="centeredButton">'+
    '<button class="custom btn btn-sm btn-default" value="A">'+n+'A</button>'+
    '<button class="custom btn btn-sm btn-default" value="B">'+n+'B</button>'+
    '<button class="custom btn btn-sm btn-default" value="C">'+n+'C</button>'+
    '<button class="custom btn btn-sm btn-default" value="D">'+n+'D</button>'+
    '<button class="custom btn btn-sm btn-default" value="E">'+n+'E</button>'+
    '</div>'+
    '<hr />';
  }else{
    console.log("not a number");
  }
  customize= customize+
      '<label>'+reduceportion+' '+n+'</label><br />'+
      '<div class="centeredButton">'+
      '<button class="delete btn btn-sm btn-danger" value="delete">'+deletes+'</button>'+
      '</div>';

  console.log(customize);
  $(element).popover({"content":customize,"html":true,"placement":"auto bottom"});
  $(element).popover('show');
  $(".custom").click(function(){
        console.log("PREMUTO UN BOTTONE SPECIALE");
        $("button[data-original-title]").popover('hide');
        var nsel=n+$(this).val();
        addOrder(null,nsel);
	});
  $(".delete").click(function(){
        $("button[data-original-title]").popover('hide');
        removeOrder(n);
	});
}
function addEventButton(max_number){
	$(".addOrder"+max_number).on("tap",function(event){
    console.log("cliccato");
		var n=$(this).val();
		addOrder(this,n);
		event.preventDefault();
	}).on("taphold",function(event){
		var n=$(this).val();
		selectLetterModal(this,n);
		event.preventDefault();
	});
}
function addMoreNumber(max_number,more_number){
	for(var n=max_number;n<(max_number+more_number);n++){
		$("#numbers").append('<button class="maki addOrder'+max_number+'" value="'+n+'" >'+n+'<span id="small'+n+'" class="quantity">X'+0+'</span></button>');
	}
	addEventButton(max_number);
	return max_number+more_number;
}

function manageDisposePopover(){
  $('body').on('tap', function (e) {
      console.log(e.target.id);
      if(e.target.id !== "share")
        $("button[data-original-title]").popover('hide');
  });
}
function initNumber(key){
  $('#share').popover();
	console.log("prova! "+key);
	var j=0;
	for(var n=0;n<max_number;n++){
		$("#numbers").append('<button class="maki addOrder" value="'+n+'">'+n+'<span id="small'+n+'" class="quantity">X'+0+'</span></button>');
	}
	$.event.special.tap.emitTapOnTaphold = false;
	addEventButton('')
	$(".addmore").click(function(){
    console.log("done");
		max_number = addMoreNumber(max_number,40);
		getOrdering();
	});
	getOrdering();
	manageUpdate(key);
  manageDisposePopover();
}

function manageUpdate(key){
	var socket = io();
	socket.on('new', function(msg){
      console.log("ricevuto"+msg);
	  var js= JSON.parse(msg);
	  console.log(js);
	  console.log(js.s);
	  updateQuantity(js.n,js.s);
    });
	socket.emit("register",key);
}

function addElementButton(n,s){
  var tempN = n.slice(0, -1);
  if($('button[value="'+tempN+'"]').length>0){
      if($('button[value="'+n+'"]').length>0){
      }else{
        var node = $('<button class="maki"></button>').addClass("addOrder"+n);
        node.attr("value",n);
        var span = $('<span class="quantity"></span>').attr("id","small"+n);
        span.text('X'+s);
        node.text(n);
        node.append(span);
        $('button[value="'+tempN+'"]').after(node);
        //$('button[value="'+tempN+'"]').after('<button class="maki addOrder'+n+'" value="'+n+'">'+n+'<span id="small'+n+'" class="quantity">X'+s+'</span></button>');
      }
  }else{
        var node = $('<button class="maki"></button>').addClass("addOrder"+n);
        node.attr("value",n);
        var span = $('<span class="quantity"></span>').attr("id","small"+n);
        span.text('X'+s);
        node.text(n);
        node.append(span);
         $("#numbers").append(node);
       //$("#numbers").append('<button class="maki addOrder'+n+'" value="'+n+'">'+n+'<span id="small'+n+'" class="quantity">X'+s+'</span></button>');
  }
}
function updateQuantity(n,s){
  console.log("aggiorno n:"+n+" s:"+s)
	if($("#small"+n).length){
		$("#small"+n).text("x"+s);
	}else{
    console.log("numero:"+n);
		if(isNaN(n)){
      addElementButton(n,s);
      addEventButton(n,key);
		}else{
			if(n>max_number){
				var q = ((parseInt(n/40)-parseInt(max_number/40))+1)*40;
				max_number=addMoreNumber(max_number,q);
			}
		}
	}
	if(s>0){
		$("#small"+n).parent().removeClass("deleted");
		$("#small"+n).parent().addClass("selected");
	}else{
		$("#small"+n).parent().removeClass("selected");
		$("#small"+n).parent().addClass("deleted");
	}
}
function getOrdering(){
	$.get("ordering",function(data){
      console.log(data);
			var js= JSON.parse(data);
			for(var i=0;i<js.length;i++){
				updateQuantity(js[i].n,js[i].s,key);
			}
	});
}


function isNewUser(){
	if (typeof(Storage) !== "undefined") {
		if (localStorage.tutorial == "true") {
		    return false;
		} else {
		    return true;
		}
	} else {
	    return true;
	}
}

function newUser(){
	if (typeof(Storage) !== "undefined") {
    	if (!localStorage.tutorial)
	       localStorage.setItem("tutorial", true);
      if (!localStorage.userkey)
         localStorage.setItem("userkey",  parseInt(Math.random()*10000));
	}
}

function getUserkey(){
  if (typeof(Storage) !== "undefined") {
    return localStorage.userkey;
  }else{
    return -1;
  }
}
